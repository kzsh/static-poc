import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import { Router } from "@reach/router";
import Home from "./Home";
import About from "./About";
import Support from "./Support";
import Dash from "./Dash";
import DashHome from "./DashHome";
import Invoices from "./Invoices";
import Team from "./Team";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <div>
            <Router>
              <Home path="/">
                <About path="about" />
                <Support path="support" />
                <Dash path="dashboard">
                  <DashHome path="/" />
                  <Invoices path="invoices" />
                  <Team path="team" />
                </Dash>
              </Home>
            </Router>
          </div>
        </header>
      </div>
    );
  }
}

export default App;
