import React from "react";
import NavLink from './NavLink'

export default props => (
  <div>
    <h2>Dash</h2>
    <ul>
      <li><NavLink partial={false} to="./">Dash Home</NavLink></li>
      <li><NavLink to="invoices">Invoices</NavLink></li>
      <li><NavLink to="team">Team</NavLink></li>
    </ul>
    {props.children}
  </div>
);
