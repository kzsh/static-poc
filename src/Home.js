import React from "react";
import NavLink from './NavLink'

export default props => (
  <div>
    <ul>
      <li><NavLink to="about">About</NavLink></li>
      <li><NavLink to="support">Support</NavLink></li>
      <li><NavLink to="dashboard">Dashboard</NavLink></li>
    </ul>
    {props.children}
    </div>
);
